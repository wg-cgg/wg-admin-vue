import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken, clearStorage, setTokenTime, removeTokenTime } from '@/utils/auth'
import router, { resetRouter } from '@/router'
import { Notification, Message } from 'element-ui'

const state = {
  token: getToken(),
  userId: '',
  name: '',
  avatar: '',
  introduction: '',
  roles: []
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_USERID: (state, userId) => {
    state.userId = userId
  }
}

const actions = {
  // 用户登录
  login({ commit }, userInfo) {
    const { username, password, code, uuid } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password, code: code, uuid: uuid }).then(response => {
        const { data } = response
        // 将后端返回的token存入vuex中
        commit('SET_TOKEN', data.token)
        // 将后端返回的token存入cookies中
        setToken(data.token)
        // 设置token过期时间
        setTokenTime(data.expireTime)
        Notification({
          title: '提示消息',
          message: '欢迎回来: ' + data.nickName,
          type: 'success',
          position: 'bottom-right',
          duration: 5 * 1000
        })
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 获取用户信息
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          reject('验证失败，请重新登录.')
        }

        const { roles, id, nickName, avatar, introduction } = data

        // roles must be a non-empty array
        if (!roles || roles.length <= 0) {
          reject('getInfo：角色必须是非空数组！')
        }
        // 把roles放入sessionStorage中
        sessionStorage.setItem('codeList', JSON.stringify(roles))

        commit('SET_ROLES', roles)
        commit('SET_NAME', nickName)
        commit('SET_AVATAR', avatar)
        commit('SET_INTRODUCTION', introduction)
        commit('SET_USERID', id)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 用户退出
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then((res) => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        // 删除token
        removeToken()
        resetRouter()
        // 重置访问过的视图和缓存的视图
        dispatch('tagsView/delAllViews', null, { root: true })
        clearStorage()
        removeTokenTime()
        resolve()
        Message({
          message: res.msg,
          type: 'success',
          duration: 5 * 1000
        })
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  },

  // dynamically modify permissions
  async changeRoles({ commit, dispatch }, role) {
    const token = role + '-token'

    commit('SET_TOKEN', token)
    setToken(token)

    const { roles } = await dispatch('getInfo')

    resetRouter()

    // generate accessible routes map based on roles
    const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true })
    // dynamically add accessible routes
    router.addRoutes(accessRoutes)

    // reset visited views and cached views
    dispatch('tagsView/delAllViews', null, { root: true })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
