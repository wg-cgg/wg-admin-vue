import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken, getTokenTime, setTokenTime, removeTokenTime, setToken, clearStorage } from '@/utils/auth'
import qs from 'qs'
import { refreshTokenApi } from '@/api/user'

// 创建一个 axios 实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// 刷新token
function reFresh() {
  const params = {
    'token': getToken()
  }
  return refreshTokenApi(params).then(res => res)
}
let isRefresh = false
// 请求拦截器
service.interceptors.request.use(
  config => {
    // 在发送请求之前做一些事情
    // 获取当前系统时间
    const currentTime = new Date().getTime()
    // 获取token过期时间
    const expireTime = getTokenTime()
    console.log('token于:' + (expireTime - currentTime) / 1000 / 60 + '分钟后过期!')
    if (expireTime > 0) {
      const minMx = (expireTime - currentTime) / 1000 / 60
      // 如果token离过期差10分钟，刷新token
      if (minMx < 10) {
        console.log('开始自动刷新token')
        if (!isRefresh) {
          isRefresh = true
          return reFresh().then(res => {
            if (res.code === 20000) {
              // 设置新的token和新的时间
              setToken(res.data.token)
              setTokenTime(res.data.expireTime)
              // 把新的token设置到头部
              config.headers.token = getToken()
            }
            return config
          }).catch(err => {
            console.log(err)
          }).finally(() => {
            isRefresh = false
          })
        }
      }
    }

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      // 在请求头中添加一个 token 属性
      config.headers['token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 20000) {
      Message({
        message: res.msg || '服务器出错!',
        type: 'error',
        duration: 5 * 1000
      })

      // 50007: 非法令牌; 50012: 密码被修改; 50008: Token 过期;
      if (res.code === 50007 || res.code === 50012 || res.code === 50008) {
        // to re-login
        MessageBox.confirm('用户登录信息过期，请重新登录！', '系统提示', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            clearStorage()
            removeTokenTime()
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.msg || '服务器出错!'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.msg || '服务器出错!',
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

// 请求方法
const http = {
  post(url, params) {
    return service.post(url, params, {
      transformRequest: [(params) => {
        return JSON.stringify(params)
      }],
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },
  put(url, params) {
    return service.put(url, params, {
      transformRequest: [(params) => {
        return JSON.stringify(params)
      }],
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },
  get(url, params) {
    return service.get(url, {
      params: params,
      paramsSerializer: (params) => {
        return qs.stringify(params)
      }
    })
  },
  getRestApi(url, params) {
    let _params
    if (Object.is(params, undefined || null)) {
      _params = ''
    } else {
      _params = '/'
      for (const key in params) {
        // console.log('请求的key为:' + key)
        // console.log('请求的value为:' + params[key])
        // eslint-disable-next-line no-prototype-builtins
        if (params.hasOwnProperty(key) && params[key] !== null && params[key] !== '') {
          _params += `${params[key]}/`
        }
      }
      // 去掉参数最后一位?
      _params = _params.substr(0, _params.length - 1)
    }
    // console.log('Restful 请求参数为:' + _params)
    if (_params) {
      return service.get(`${url}${_params}`)
    } else {
      return service.get(url)
    }
  },
  delete(url, params) {
    let _params
    if (Object.is(params, undefined || null)) {
      _params = ''
    } else {
      _params = '/'
      for (const key in params) {
        // eslint-disable-next-line no-prototype-builtins
        if (params.hasOwnProperty(key) && params[key] !== null && params[key] !== '') {
          _params += `${params[key]}/`
        }
      }
      // 去掉参数最后一位?
      _params = _params.substr(0, _params.length - 1)
    }
    if (_params) {
      return service.delete(`${url}${_params}`).catch(err => {
        Message.error(err.msg)
        return Promise.reject(err)
      })
    } else {
      return service.delete(url).catch(err => {
        Message.error(err.msg)
        return Promise.reject(err)
      })
    }
  },
  upload(url, params) {
    return service.post(url, params, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },
  login(url, params) {
    return service.post(url, params, {
      transformRequest: [(params) => {
        return qs.stringify(params)
      }],
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  }
}
export default http
