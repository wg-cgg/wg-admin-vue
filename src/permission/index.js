// 按钮权限判断
export default function hasPermission(params) {
  let flag = false
  // 从sessionStorage中获取权限信息
  const code = JSON.parse(sessionStorage.getItem('codeList'))
  for (let i = 0; i < code.length; i++) {
    if (code[i] === params) {
      flag = true
      break
    }
  }
  return flag
}
