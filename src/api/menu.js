import http from '@/utils/request'

// 获取菜单列表
export async function getMenuListApi() {
  return await http.get('/menu/getMenuList')
}
// 获取上级菜单
export async function getParentTreeApi() {
  return await http.get('/menu/getParentMenuList')
}
// 新增权限
export async function insertMenuApi(params) {
  return await http.post('/menu', params)
}
// 修改权限
export async function updateMenuApi(params) {
  return await http.put('/menu', params)
}

// 删除权限
export async function deleteMenuApi(params) {
  return await http.delete('/menu', params)
}
