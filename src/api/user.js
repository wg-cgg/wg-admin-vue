import http from '@/utils/request'
// 获取验证码图片
export async function getCaptcha() {
  return await http.get('/user/captcha', null)
}

// 用户登录
export async function login(data) {
  return await http.login('/user/login', data)
}

// 获取用户信息
export async function getInfo() {
  return await http.get('/user/getUserInfo')
}

// 刷新token
export async function refreshTokenApi(params) {
  return await http.post('/user/refreshToken', params)
}

// 获取菜单列表
export async function getMenuList() {
  return await http.get('/user/getMenuList')
}

// 退出登录
export async function logout() {
  return await http.post('/user/logout')
}

// 获取用户列表
export async function getUserListApi(params) {
  return await http.get('/user/getUserList', params)
}

// 根据id获取用户
export async function getUserByIdApi(params) {
  return await http.getRestApi('/user/getUserById', params)
}

// 新增用户
export async function addUserApi(params) {
  return await http.post('/user', params)
}

// 更新用户
export async function editUserApi(params) {
  return await http.put('/user', params)
}

// 删除用户
export async function deleteUserApi(params) {
  return await http.delete('/user', params)
}

// 获取分配角色角色列表
export async function assignRoleListApi(params) {
  return await http.get('/user/getRoleListForAssign', params)
}

// 获取用户的角色id
export async function getRoleIdByUserIdApi(params) {
  return await http.getRestApi('/user/getRoleByUserId', params)
}

// 分配角色保存
export async function assignRoleSaveApi(params) {
  return await http.post('/user/assignRole', params)
}
