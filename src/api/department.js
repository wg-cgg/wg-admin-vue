import http from '@/utils/request'

// 获取机构列表
export async function getDepartmentList() {
  return await http.get('/department/getDepartmentList', null)
}
