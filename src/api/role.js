import http from '@/utils/request'

// 获取角色列表
export async function getRoleListApi(params) {
  return await http.get('/role/getRoleList', params)
}

// 新增角色
export async function insertRoleApi(params) {
  return await http.post('/role', params)
}

// 编辑角色
export async function updateRoleApi(params) {
  return await http.put('/role', params)
}

// 删除角色
export async function deleteRoleApi(params) {
  return await http.delete('/role', params)
}

// 获取分配权限树数据
export async function getAssignTreeApi(params) {
  return await http.get('/role/getAssignPermissionTree', params)
}

// 分配权限保存
export async function assignSaveApi(params) {
  return await http.post('/role/roleAssignSave', params)
}
